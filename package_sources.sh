#!/bin/bash

VERSION=5.1804

pushd sources
tar cvzf ../deskos-release-7-${VERSION}.tar.gz deskos-release-7
popd
